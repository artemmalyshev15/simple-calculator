function goToResult() {
    if ((document.getElementById('first_value').value === "") || (document.getElementById('second_value').value === "")) {
        alert('Поля ввода значений пусты! Заполните поля!');
        event.preventDefault();
    } else if (!(
        (document.getElementById('plus').checked)
        || (document.getElementById('minus').checked)
        || (document.getElementById('multiplication').checked)
        || (document.getElementById('division').checked))) {
        alert('Операция не выбрана! Выберите операцию!');
        event.preventDefault();
    } else {
        var value1 = document.getElementById('first_value').value;
        var value2 = document.getElementById('second_value').value;
        var result;
        var operation = document.querySelector('input[name="operation"]:checked').value;
        if (operation == '+') {
            value1 = +value1;
            value2 = +value2;
            result = value1 + value2;
            document.getElementById("result").value = result;
        } else if (operation == '-') {
            result = value1 - value2;
            document.getElementById("result").value = result;
        } else if (operation == '*') {
            result = value1 * value2;
            document.getElementById("result").value = result;
        } else if ((operation == '/') && (value2 !== "0")) {
            result = value1 / value2;
            document.getElementById("result").value = result;
        } else {
            alert('Деление на ноль! На ноль делить нельзя!');
            event.preventDefault();
        }
    }
}

function getQueryParameters() {
    let params = new URLSearchParams(document.location.search);
    document.getElementById("first_value").innerHTML = params.get("first_value");
    document.getElementById("second_value").innerHTML = params.get("second_value");

    document.getElementById("result").innerHTML = params.get("result");

    document.getElementById("operation").innerHTML = params.get("operation");
    switch (params.get("operation")) {
        case '+':
            document.getElementById("operation").innerHTML = 'Сложение';
            break;
        case '-':
            document.getElementById("operation").innerHTML = 'Вычитание';
            break;
        case '*':
            document.getElementById("operation").innerHTML = 'Умножение';
            break;
        case '/':
            document.getElementById("operation").innerHTML = 'Деление';
            break;
    }

}

